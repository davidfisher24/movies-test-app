<?php

namespace App\Http\Controllers;
use App\MovieDbApi;

class MoviesController extends Controller
{

	private $movieDbApi;

	public function __construct(MovieDbApi $movieDbApi)
	{
		$this->movieDbApi = $movieDbApi;
	}

    public function listAction($page = 1)
    {
		$movies = $this->movieDbApi->getPage($page);
        return view('movies.index', compact('movies', 'page'));
    }	

    public function oneAction($id)
    {
		$movie = $this->movieDbApi->getMovie($id);
        return view('movies.page', compact('movie'));
    }
}
