<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Bookmark;
use App\MovieDbApi;

class BookmarkController extends Controller
{

	private $movieDbApi;

	public function __construct(MovieDbApi $movieDbApi)
	{
		$this->movieDbApi = $movieDbApi;
	}

    public function allAction()
    {
    	$bookmarks = Auth::user()->bookmarks()->orderBy('rating', 'DESC')->get();
        return view('bookmarks.index', compact('bookmarks'));
    }

    public function addAction($movieId)
    {
    	$movie = $this->movieDbApi->getMovie($movieId);

    	$user = Auth::user();
		$bookmark = new Bookmark();
		$bookmark->movie_id = $movieId;
		$bookmark->movie_title = $movie['title'];
        $bookmark->rating = $movie['vote_average'];
		$user->bookmarks()->save($bookmark);

		$bookmarks = Auth::user()->bookmarks()->get();
        return view('bookmarks.index', compact('bookmarks'));
    }

    public function removeAction($movieId)
    {
    	$user = Auth::user();
    	$user->bookmarks()->where('movie_id',$movieId)->delete();

    	$bookmarks = Auth::user()->bookmarks()->get();
        return view('bookmarks.index', compact('bookmarks'));
    }
}
