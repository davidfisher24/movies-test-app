<?php

namespace App;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redis;

class MovieDbApi
{
    protected $baseUrl;
    protected $apiKey;
    protected $client;
    
    function __construct(Client $client)
    {
        $this->client = $client;
        $this->baseUrl = env('MOVIE_DB_BASE_URL');
        $this->apiKey = env('MOVIE_DB_API_KEY');
    }

    public function getPage($page) {
        $data = $this->endpointRequest('/movie/top_rated?page='.$page. '&api_key=' . $this->apiKey);

        $movies = $data['results'];
        $redis = new Redis();
        Redis::pipeline(function ($pipe) use ($movies) {
            foreach ($movies as $movie) {
                $pipe->hmset('movies',array($movie['id'] => json_encode($movie)));
            }
        });
        return $movies;
    }

    public function getMovie($id) {
        $redis = new Redis();
        if ($movie = Redis::hget('movies',$id)) {
            return json_decode($movie, true);
        } else {
            return $this->endpointRequest('/movie/'.$id. '?api_key=' . $this->apiKey);
        }
    }

    private function endpointRequest($url)
    {
        try {
            $url = $this->baseUrl . $url;
            $response = $this->client->request('GET', $url);
        } catch (\Exception $e) {
            return [];
        }

        return $this->response_handler($response);
    }

    private function response_handler($response)
    {
        if ($response) {
            return json_decode($response->getBody(), true);
        }
        
        return [];
    }
}