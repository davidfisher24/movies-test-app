<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');

Route::get('/', 'MoviesController@listAction')->name('movies');
Route::get('/movies/{page}', 'MoviesController@listAction');
Route::get('/movie/{id}', 'MoviesController@oneAction')->name('movie');

Route::get('/home', 'BookmarkController@allAction')->name('bookmarks')->middleware('auth');
Route::get('/add/{movieId}', 'BookmarkController@addAction')->middleware('auth');
Route::get('/remove/{movieId}', 'BookmarkController@removeAction')->middleware('auth');
