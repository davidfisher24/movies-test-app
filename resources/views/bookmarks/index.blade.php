@extends('layouts.app')

@section('content')

<div class="bs-example container" data-example-id="striped-table">
	<h2> My Bookmarks </h2>

	@if (count($bookmarks) === 0) 
		<p>You don't have any bookmarks yet :(</p>
	@else
		<div class="list-group">
			<ul class="list-group">
				@foreach($bookmarks as $i => $bookmark)
				<li class="list-group-item">
					<h5 class="mb-1">{{$bookmark['movie_title']}}</h5>
					<small>{{$bookmark['created_at']->diffForHumans()}}</small>
					<a href="/movie/{{$bookmark['movie_id']}}">View</a>
					<p class="float-right ">{{$bookmark['rating']}}</p>
				</li>
				@endforeach
			</ul>
		</div>
	@endif

	
</div>

@endsection