@extends('layouts.app')

@section('content')

<div class="bs-example container">
	<div class="card-columns">

		<div class="card" style="width: 18rem;">
			<img src="https://image.tmdb.org/t/p/w500/{{$movie['poster_path']}}" class="card-img-top" alt="...">
			<div class="card-body">
			    <h5 class="card-title">{{$movie['title']}}</h5>
			</div>
		</div>

		<div class="card" style="width: 18rem;">
		  <div class="card-body">
		    <p class="card-text">{{$movie['overview']}}</p>
		  </div>
		  <ul class="list-group list-group-flush">
		    <li class="list-group-item">
		    	<b>Released: </b>{{$movie['release_date']}}
		    </li>
		    <li class="list-group-item">
		    	<b>Original Title: </b>{{$movie['original_title']}}
		    </li>
		    <li class="list-group-item">
		    	<b>Average score: </b>{{$movie['vote_average']}}
		    </li>
		  </ul>

		  @if (Auth::check())
			@if (null === Auth::user()->bookmarks()->where('movie_id',$movie['id'])->first())
			<a type="button" href="/add/{{$movie['id']}}" class="btn btn-labeled btn-primary">
	                <span class="btn-label"><i class="glyphicon glyphicon-bookmark"></i></span>Add as bookmark</a>
	        @else
        	<a type="button" href="/remove/{{$movie['id']}}" class="btn btn-labeled btn-danger">
	                <span class="btn-label"><i class="glyphicon glyphicon-bookmark"></i></span>Remove bookmark</a>
	        @endif
		@endif
		</div>
	</div>
</div>

@endsection