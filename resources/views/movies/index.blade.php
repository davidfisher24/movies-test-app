@extends('layouts.app')

@section('content')

<div class="bs-example container" data-example-id="striped-table">
	<nav aria-label="Page navigation">
		<ul class="pagination">
			@for ($i = 1; $i <= 5; $i++)
		        <li class="page-item">
		        	<a class="page-link btn {{ $i == $page ? 'btn-primary disabled' : ''  }}" href="/movies/{{$i}}">
		        		{{$i * 20 - 19}}-{{$i * 20}}
		        	</a>
		        </li>
		    @endfor

		</ul>
	</nav>

  <table class="table table-striped table-bordered table-hover">
    <caption>Top Rated Movies</caption>
    <thead>
      <tr>
        <th>#</th>
        <th>Movie</th>
        <th>Date</th>
        <th>Rating</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
    @foreach($movies as $i => $movie)
		<tr>
		    <th scope="row">{{($i+1)+(($page -1)*20)}}</th>
	        <td>{{ $movie['title'] }}</td>
	        <td>{{ $movie['release_date'] }}</td>
	        <td>{{ $movie['vote_average'] }}</td>
	        <td>
	        	<a href="/movie/{{$movie['id']}}">
	        		more
	        	</a>
	        </td>
		</tr>
	@endforeach
    </tbody>
  </table>
</div>

@endsection